@echo off
set busy=1

:start
echo ========================================================
echo KIOSK  - With VLS Ghent libs...   - %busy%
pause
set Destination="C:\projects\Kiosk-out"
set KioskRepo = "C:\projects\kiosk"
set ProjectRepo = "C:\projects\vls-ghent"

cd %Destination%
mkdir "%Destination%\Arrival Kiosk"
mkdir "%Destination%\Arrival Kiosk\Components"

del *.* /s /f /q


echo Arrival Kiosk

copy "C:\projects\Kiosk\Agidens.Aline.Kiosk\Agidens.Aline.Kiosk.Shell\bin\Debug\*" "C:\projects\Kiosk-out\Arrival Kiosk\*"

copy "C:\projects\Kiosk\Agidens.Aline.Kiosk\Agidens.Aline.Kiosk.Screens\bin\Debug\Agidens.Aline.Kiosk.Screens.dll" "C:\projects\Kiosk-out\Arrival Kiosk\Components\Agidens.Aline.Kiosk.Screens.dll"
copy "C:\projects\Kiosk\Agidens.Aline.Kiosk\Agidens.Aline.Kiosk.Screens\bin\Debug\Agidens.Aline.Kiosk.Screens.pdb" "C:\projects\Kiosk-out\Arrival Kiosk\Components\Agidens.Aline.Kiosk.Screens.pdb"

copy "C:\projects\vls-ghent\Vls.Ghent\Vls.Ghent.Kiosk\bin\Debug\Vls.Ghent.Kiosk.dll" "C:\projects\Kiosk-out\Arrival Kiosk\Components\Vls.Ghent.Kiosk.dll"
copy "C:\projects\vls-ghent\Vls.Ghent\Vls.Ghent.Kiosk\bin\Debug\Vls.Ghent.Kiosk.pdb" "C:\projects\Kiosk-out\Arrival Kiosk\Components\Vls.Ghent.Kiosk.pdb"

copy "C:\projects\vls-ghent\Configuration\Local\Kiosk\Kiosk Arrival Flow\FlowConfig.xml" "C:\projects\Kiosk-out\Arrival Kiosk\FlowConfig.xml"
copy "C:\projects\vls-ghent\Configuration\Local\Kiosk\Kiosk Arrival Flow\Agidens.Aline.Kiosk.Shell.exe.config" "C:\projects\Kiosk-out\Arrival Kiosk\Agidens.Aline.Kiosk.Shell.exe.config"

echo Terminal Entry Kiosk
:: copy dlls
copy "C:\projects\Kiosk\Agidens.Aline.Kiosk\Agidens.Aline.Kiosk.Shell\bin\Debug\*" "C:\projects\Kiosk-out\Terminal Entry Kiosk\*"

copy "C:\projects\Kiosk\Agidens.Aline.Kiosk\Agidens.Aline.Kiosk.Screens\bin\Debug\Agidens.Aline.Kiosk.Screens.dll" "C:\projects\Kiosk-out\Terminal Entry Kiosk\Components\Agidens.Aline.Kiosk.Screens.dll"
copy "C:\projects\Kiosk\Agidens.Aline.Kiosk\Agidens.Aline.Kiosk.Screens\bin\Debug\Agidens.Aline.Kiosk.Screens.pdb" "C:\projects\Kiosk-out\Terminal Entry Kiosk\Components\Agidens.Aline.Kiosk.Screens.pdb"

copy "C:\projects\vls-ghent\Vls.Ghent\Vls.Ghent.Kiosk\bin\Debug\Vls.Ghent.Kiosk.dll" "C:\projects\Kiosk-out\Terminal Entry Kiosk\Components\Vls.Ghent.Kiosk.dll"
copy "C:\projects\vls-ghent\Vls.Ghent\Vls.Ghent.Kiosk\bin\Debug\Vls.Ghent.Kiosk.pdb" "C:\projects\Kiosk-out\Terminal Entry Kiosk\Components\Vls.Ghent.Kiosk.pdb"

copy "C:\projects\vls-ghent\Configuration\Local\Kiosk\Kiosk Terminal Entry\FlowConfig.xml" "C:\projects\Kiosk-out\Terminal Entry Kiosk\FlowConfig.xml"
copy "C:\projects\vls-ghent\Configuration\Local\Kiosk\Kiosk Terminal Entry\Agidens.Aline.Kiosk.Shell.exe.config" "C:\projects\Kiosk-out\Terminal Entry Kiosk\Agidens.Aline.Kiosk.Shell.exe.config"

::Preparation kiosk
echo Preparation Kiosk
:: copy dlls
copy "C:\projects\Kiosk\Agidens.Aline.Kiosk\Agidens.Aline.Kiosk.Shell\bin\Debug\*" "C:\projects\Kiosk-out\Preparation Kiosk\*"

copy "C:\projects\Kiosk\Agidens.Aline.Kiosk\Agidens.Aline.Kiosk.Screens\bin\Debug\Agidens.Aline.Kiosk.Screens.dll" "C:\projects\Kiosk-out\Preparation Kiosk\Components\Agidens.Aline.Kiosk.Screens.dll"
copy "C:\projects\Kiosk\Agidens.Aline.Kiosk\Agidens.Aline.Kiosk.Screens\bin\Debug\Agidens.Aline.Kiosk.Screens.pdb" "C:\projects\Kiosk-out\Preparation Kiosk\Components\Agidens.Aline.Kiosk.Screens.pdb"

copy "C:\projects\vls-ghent\Vls.Ghent\Vls.Ghent.Kiosk\bin\Debug\Vls.Ghent.Kiosk.dll" "C:\projects\Kiosk-out\Preparation Kiosk\Components\Vls.Ghent.Kiosk.dll"
copy "C:\projects\vls-ghent\Vls.Ghent\Vls.Ghent.Kiosk\bin\Debug\Vls.Ghent.Kiosk.pdb" "C:\projects\Kiosk-out\Preparation Kiosk\Components\Vls.Ghent.Kiosk.pdb"

copy "C:\projects\vls-ghent\Configuration\Local\Kiosk\Kiosk Preparation\FlowConfig.xml" "C:\projects\Kiosk-out\Preparation Kiosk\FlowConfig.xml"
copy "C:\projects\vls-ghent\Configuration\Local\Kiosk\Kiosk Preparation\Agidens.Aline.Kiosk.Shell.exe.config" "C:\projects\Kiosk-out\Preparation Kiosk\Agidens.Aline.Kiosk.Shell.exe.config"

::Weighbridge kiosk
echo Weighbridge Kiosk
:: copy dlls
copy "C:\projects\Kiosk\Agidens.Aline.Kiosk\Agidens.Aline.Kiosk.Shell\bin\Debug\*" "C:\projects\Kiosk-out\Weighbridge Kiosk\*"

copy "C:\projects\Kiosk\Agidens.Aline.Kiosk\Agidens.Aline.Kiosk.Screens\bin\Debug\Agidens.Aline.Kiosk.Screens.dll" "C:\projects\Kiosk-out\Weighbridge Kiosk\Components\Agidens.Aline.Kiosk.Screens.dll"
copy "C:\projects\Kiosk\Agidens.Aline.Kiosk\Agidens.Aline.Kiosk.Screens\bin\Debug\Agidens.Aline.Kiosk.Screens.pdb" "C:\projects\Kiosk-out\Weighbridge Kiosk\Components\Agidens.Aline.Kiosk.Screens.pdb"

copy "C:\projects\vls-ghent\Vls.Ghent\Vls.Ghent.Kiosk\bin\Debug\Vls.Ghent.Kiosk.dll" "C:\projects\Kiosk-out\Weighbridge Kiosk\Components\Vls.Ghent.Kiosk.dll"
copy "C:\projects\vls-ghent\Vls.Ghent\Vls.Ghent.Kiosk\bin\Debug\Vls.Ghent.Kiosk.pdb" "C:\projects\Kiosk-out\Weighbridge Kiosk\Components\Vls.Ghent.Kiosk.pdb"

copy "C:\projects\vls-ghent\Configuration\Local\Kiosk\Kiosk WB out\FlowConfig.xml" "C:\projects\Kiosk-out\Weighbridge Kiosk\FlowConfig.xml"
copy "C:\projects\vls-ghent\Configuration\Local\Kiosk\Kiosk WB out\Agidens.Aline.Kiosk.Shell.exe.config" "C:\projects\Kiosk-out\Weighbridge Kiosk\Agidens.Aline.Kiosk.Shell.exe.config"

::Terminal Exit kiosk
echo Terminal Exit Kiosk
:: copy dlls
copy "C:\projects\Kiosk\Agidens.Aline.Kiosk\Agidens.Aline.Kiosk.Shell\bin\Debug\*" "C:\projects\Kiosk-out\Terminal Exit Kiosk\*"

copy "C:\projects\Kiosk\Agidens.Aline.Kiosk\Agidens.Aline.Kiosk.Screens\bin\Debug\Agidens.Aline.Kiosk.Screens.dll" "C:\projects\Kiosk-out\Terminal Exit Kiosk\Components\Agidens.Aline.Kiosk.Screens.dll"
copy "C:\projects\Kiosk\Agidens.Aline.Kiosk\Agidens.Aline.Kiosk.Screens\bin\Debug\Agidens.Aline.Kiosk.Screens.pdb" "C:\projects\Kiosk-out\Terminal Exit Kiosk\Components\Agidens.Aline.Kiosk.Screens.pdb"

copy "C:\projects\vls-ghent\Vls.Ghent\Vls.Ghent.Kiosk\bin\Debug\Vls.Ghent.Kiosk.dll" "C:\projects\Kiosk-out\Terminal Exit Kiosk\Components\Vls.Ghent.Kiosk.dll"
copy "C:\projects\vls-ghent\Vls.Ghent\Vls.Ghent.Kiosk\bin\Debug\Vls.Ghent.Kiosk.pdb" "C:\projects\Kiosk-out\Terminal Exit Kiosk\Components\Vls.Ghent.Kiosk.pdb"

copy "C:\projects\vls-ghent\Configuration\Local\Kiosk\Kiosk Terminal Exit\FlowConfig.xml" "C:\projects\Kiosk-out\Terminal Exit Kiosk\FlowConfig.xml"
copy "C:\projects\vls-ghent\Configuration\Local\Kiosk\Kiosk Terminal Exit\Agidens.Aline.Kiosk.Shell.exe.config" "C:\projects\Kiosk-out\Terminal Exit Kiosk\Agidens.Aline.Kiosk.Shell.exe.config"


::Call Off Kiosk
echo Call Off Kiosk
:: copy dlls
copy "C:\projects\Kiosk\Agidens.Aline.Kiosk\Agidens.Aline.Kiosk.Shell\bin\Debug\*" "C:\projects\Kiosk-out\Call Off Kiosk\*"

copy "C:\projects\Kiosk\Agidens.Aline.Kiosk\Agidens.Aline.Kiosk.Screens\bin\Debug\Agidens.Aline.Kiosk.Screens.dll" "C:\projects\Kiosk-out\Call Off Kiosk\Components\Agidens.Aline.Kiosk.Screens.dll"
copy "C:\projects\Kiosk\Agidens.Aline.Kiosk\Agidens.Aline.Kiosk.Screens\bin\Debug\Agidens.Aline.Kiosk.Screens.pdb" "C:\projects\Kiosk-out\Call Off Kiosk\Components\Agidens.Aline.Kiosk.Screens.pdb"

copy "C:\projects\vls-ghent\Vls.Ghent\Vls.Ghent.Kiosk\bin\Debug\Vls.Ghent.Kiosk.dll" "C:\projects\Kiosk-out\Call Off Kiosk\Components\Vls.Ghent.Kiosk.dll"
copy "C:\projects\vls-ghent\Vls.Ghent\Vls.Ghent.Kiosk\bin\Debug\Vls.Ghent.Kiosk.pdb" "C:\projects\Kiosk-out\Call Off Kiosk\Components\Vls.Ghent.Kiosk.pdb"

copy "C:\projects\vls-ghent\Configuration\Local\Kiosk\Kiosk Call-Off\FlowConfig.xml" "C:\projects\Kiosk-out\Call Off Kiosk\FlowConfig.xml"
copy "C:\projects\vls-ghent\Configuration\Local\Kiosk\Kiosk Call-Off\Agidens.Aline.Kiosk.Shell.exe.config" "C:\projects\Kiosk-out\Call Off Kiosk\Agidens.Aline.Kiosk.Shell.exe.config"




set /A busy = busy + 1
goto start