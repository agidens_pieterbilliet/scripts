@echo off

set Source-BE="C:\projects\aline\"
set Source-OM="C:\projects\aline.ordermanager\"
set Source-Epia="C:\projects\Epia\"

set Destination="C:\projects\OrderManager-out\"

set busy=1

:start
echo %~f0
echo Start OrderManager - Server %busy%
pause

taskkill /F /IM Egemin.Epia.Server.exe


cd "%Source-OM%"

:: copy Epia assemblies
xcopy "%Source-Epia%bin\Any CPU\Debug" "%Destination%" /s /q /f /d /e /r /y /k

:: copy 
xcopy "%Source-OM%0-Configuration\ProfileCatalog.xml" "%Destination%\Data\ProfileCatalog" /s /q /f /d /e /r /y /k
xcopy "%Source-OM%3-Modules\Aline.OrderManager.Core.Data\bin\Debug\*.dll" "%Destination%" /s /q /f /d /e /r /y /k
xcopy "%Source-OM%3-Modules\Aline.OrderManager.Core.Services\bin\Debug\*.dll" "%Destination%" /s /q /f /d /e /r /y /k
xcopy "%Source-OM%3-Modules\Aline.OrderManager.Core.UiComponents\bin\Debug\*.dll" "%Destination%" /s /q /f /d /e /r /y /k
xcopy "%Source-OM%3-Modules\Aline.OrderManager.Core.UiComponents\bin\Debug\Resources\*" "%Destination%\Resources" /s /q /f /e /r /y /k
xcopy "%Source-OM%3-Modules\Aline.OrderManager.Core.UiComponents\bin\Debug\Data\Resources\*" "%Destination%\Data\Resources" /s /q /f /e /r /y /k
xcopy "%Source-OM%3-Modules\Aline.OrderManager.Core.UiComponents\Data\Security\*" "%Destination%\Data\Security" /s /q /f /e /r /y /k
xcopy "%Source-OM%3-Modules\Aline.Contract.UiComponents\bin\Debug\*.dll" "%Destination%" /s /q /f /d /e /r /y /k

:: copy assemblies shared with Aline Back-End 
xcopy "%Source-BE%Agidens.MessageBroker.Msg\bin\Debug\*.dll" "%Destination%" /s /q /f /d /e /r /y /k
xcopy "%Source-BE%Aline.Resource.Service\bin\Debug\*.dll" "%Destination%" /s /q /f /d /e /r /y /k
xcopy "%Source-BE%Agidens.Terminal.Suite.Service\bin\Debug\*.dll" "%Destination%" /s /q /f /d /e /r /y /k

:: set config to TOV specifics
xcopy "%Source-OM%0-Configuration\*.config" "%Destination%" /s /q /f /e /r /y /k
xcopy "c:\projects\tov\Configuration\Local\Portal\*" "%Destination%" /s /q /f /d /e /r /y /k

::start epia server
cd %Destination%
start Egemin.Epia.Server.exe

set /A busy = busy + 1
goto start