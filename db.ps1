new-object System.String('-', 50)
"Re-Creating EvitaDev db:"

Get-ChildItem "C:\projects\Aline\1 Deployment\1 Datastore\" | Where-Object {$_.Extension -eq ".sql"} | 
Foreach-Object {
    $_.FullName
    Invoke-Sqlcmd -InputFile $_.FullName -ServerInstance .\MSSQLSERVER2016 
}

new-object System.String('-', 50)
"MigrationsV2:"

Get-ChildItem "C:\projects\Aline\1 Deployment\1 Datastore\MigrationsV2" | Where-Object {$_.Extension -eq ".sql"} | Sort-Object -Property FullName |
Foreach-Object {
    $_.FullName
    Invoke-Sqlcmd -InputFile $_.FullName -ServerInstance .\MSSQLSERVER2016 
}

new-object System.String('-', 50)
"Done!"