﻿param(
    [switch]$clean = $false,
    [switch]$build = $false,
    [switch]$copy = $false,
    [switch]$start = $false,
    [switch]$server = $false,
    [switch]$verbose = $false
    )


$Destination = "C:\projects\OrderManager-out"
$Source_OM = "C:\projects\aline.ordermanager"
$Source_Epia = "C:\projects\epia"

function Invoke-VsMSBuildCmd() {

$VSSetupExists = Get-Command Get-VSSetupInstance -ErrorAction SilentlyContinue
if(-not $VSSetupExists)
{ Install-Module VSSetup -Scope CurrentUser -Force }

$vsPath = (Get-VSSetupInstance | Select-VSSetupInstance -Latest -Require Microsoft.Component.MSBuild).InstallationPath

$vsMSBuildCmdPath = (Get-ChildItem $vsPath -Recurse -Filter "VsMSBuildCmd.bat"| Select-Object -first 1).FullName

Invoke-Item -Path $vsMSBuildCmdPath
}



if($build)
{
    Write-Output "building Aline Order Manager solution..."
    Push-Location -Path $Source_OM
    Invoke-VsMSBuildCmd
    & "msbuild Aline.OrderManager.sln"
    Pop-Location
}


if($clean)
{
    Write-Output "Cleaning $Destination ..."
    Remove-Item -Path $Destination -Force -Recurse
    New-Item -ItemType Directory -Force -Path $Destination
}

if($copy)
{

$CopyParams = @{            
    Recurse = $true
     Force = $true
     Verbose = $verbose
} 

    Write-Output "Copying into $Destination ..."
    
    Copy-Item -Path "$Source_Epia\bin\Any CPU\Debug\*" -Destination "$Destination" @CopyParams
        
    Copy-Item -Path "$Source_OM\0-Configuration\ProfileCatalog.xml" -Destination "$Destination\Data\ProfileCatalog" @CopyParams
    Copy-Item -Path "$Source_OM\0-Configuration\*.config" -Destination "$Destination" @CopyParams

    Copy-Item -Path "$Source_OM\3-Modules\Aline.OrderManager.Core.Data\bin\Debug\*.dll" -Destination "$Destination" @CopyParams
    Copy-Item -Path "$Source_OM\3-Modules\Aline.OrderManager.Core.Services\bin\Debug\*.dll" -Destination "$Destination" @CopyParams
    Copy-Item -Path "$Source_OM\3-Modules\Aline.OrderManager.Core.UiComponents\bin\Debug\*.dll" -Destination "$Destination" @CopyParams
    Copy-Item -Path "$Source_OM\3-Modules\Aline.OrderManager.Core.UiComponents\bin\Debug\Resources\*" -Destination "$Destination\Resources" @CopyParams
    Copy-Item -Path "$Source_OM\3-Modules\Aline.OrderManager.Core.UiComponents\bin\Debug\Data\Resources\*" -Destination "$Destination\Data\Resources" @CopyParams
    Copy-Item -Path "$Source_OM\3-Modules\Aline.OrderManager.Core.UiComponents\bin\Debug\Data\Security\*" -Destination "$Destination\Data\Security" @CopyParams

    Copy-Item -Path "$Source_OM\3-Modules\Aline.TimeSheet.Data\bin\Debug\*.dll" -Destination "$Destination" @CopyParams

    Copy-Item -Path "$Source_OM\3-Modules\Aline.TimeSheet.Services\bin\Debug\*.dll" -Destination "$Destination" @CopyParams
    Copy-Item -Path "$Source_OM\3-Modules\Aline.TimeSheet.UiComponents\bin\Debug\*.dll" -Destination "$Destination" @CopyParams
    Copy-Item -Path "$Source_OM\3-Modules\Aline.TimeSheet.UiComponents\bin\Debug\Resources\*" -Destination "$Destination\Resources" @CopyParams
    Copy-Item -Path "$Source_OM\3-Modules\Aline.TimeSheet.UiComponents\bin\Debug\Data\Resources\*" -Destination "$Destination\Data\Resources" @CopyParams
    
#    Copy-Item -Path "$Source_OM\3-Modules\Aline.TimeSheet.UiComponents\bin\Debug\Data\Security\*" -Destination "$Destination\Data\Security" -Recurse -Force  


}


if($start)
{
    Push-Location -Path $Destination
    Write-Output "Starting Agidens Epia Server..."
    Invoke-Item Egemin.Epia.Server.exe 
    Write-Output "Starting Agidens Epia Shell..."
    Invoke-Item Egemin.Epia.Shell.exe
    Pop-Location
}

if($server)
{
    Push-Location -Path $Destination
    Write-Output "Starting Agidens Epia Server..."
    Invoke-Item Egemin.Epia.Server.exe 
    Pop-Location
}


function get-MsBuildPath() {

$VSSetupExists = Get-Command Get-VSSetupInstance -ErrorAction SilentlyContinue
if(-not $VSSetupExists)
{ Install-Module VSSetup -Scope CurrentUser -Force }
$vsPath = (Get-VSSetupInstance | Select-VSSetupInstance -Latest -Require Microsoft.Component.MSBuild).InstallationPath

(Get-ChildItem $vsPath -Recurse -Filter "msbuild.exe"| Select-Object -first 1).FullName
}
